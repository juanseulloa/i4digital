### Prueba Desarrollador I4DIGITAL
#### Contexto

En la presente prueba se busca medir las habilidades técnicas, experiencia y conocimientos adquiridos por el próximo desarrollador de software. Persona clave que hará parte de un equipo de trabajo y del cual se espera un gran desempeño.
#### 1. Descargar el codigo fuente en formato .zip
Despues de descargar el proyecto, se sugiere abrir el codigo en un editor como Visual Studio Code o el de su preferencia.
  
#### 2. Estructura del proyecto 
 + documentation
 + externalresources
 +  src
    + archivo
        + reportes
    + configuracion
        + api
        + conexion
		+ dominios
    + controladores
        + foto
	    + peticion
	    + publicacion
	    + usuario
    + daos
        + foto
	    + peticion
	    + publicacion
	    + usuario
	+ repositorios
    + rutas
    + index.ts
 + test

#### 3. Base de datos postgreSQL  

##### 3.1 Creación usuario
     nombre usuario:  user_i4digital
     contraseña : laClavei4digital
     nota: recuerde asignar permisos de conexion al usuario
##### 3.2 Creación base de datos
     nombre base de datos: db_i4digital
     propietario (owner): user_i4digital
 
##### 3.3 Ejecutar el script ddl que se encuentra en la carpeta recursos externos:
`2_ddl_postgresql.sql o copiar el siguiente codigo:`

```
	create table peticiones (
	cod_peticion         serial               not null,
	fecha_peticion       date                 not null,
	metodo_peticion      varchar(200)         not null,
	datos_peticion       text                 not null,
	constraint pk_peticiones primary key (cod_peticion)
	);
	alter table peticiones owner to user_i4digital;
```
	
#### 4. Verificar versión nodeJS
 La api fue construida con la versión 16.13.1
 si requiere consultar su versión de nodejs, ejecute en una terminal el siguiente comando:
 
 `$ node  --version`

#### 5. Verificar versión TypeScript
 La api fue construida con la versión 4.5.4
 si requiere consultar su versión de typescript, ejecute en una terminal el siguiente comando:

`$ tsc  --version`

#### 6.  Paquetes uilizados:

    npm i axios
    npm i cors
    npm i express
    npm i exceljs
    npm i pg-promise
    npm i mocha
    npm i morgan
    npm i nanoid

    npm i  nodemon --save-dev
	npm i  ts-node --save-dev
    npm i  supertest --save-dev
	npm i  @types/cors  --save-dev
    npm i  @types/express --save-dev
    npm i  @types/mocha --save-dev
    npm i  @types/morgan --save-dev
    npm i  @types/supertest --save-dev

 #### 7. Habilitar la api con sus servicios
 +  7.1 En una terminal  dentro del proyecto ejecutar comando (instalación paquetes):
    
	`$ npm i`
   
+ 7.2 En dos terminales independientes dentro del proyecto, ejecutar los siguientes scripts en el siguiente orden:
  
  `$ npm run build`
  
  `$ npm run dev `


#### 8. Documentación generada por swagger editor:	
 http://localhost:3210/i4digital/documentation

#### 9. Prueba de la api y todos sus servicios (Postman): 
  `9.1 listar usuarios:`  `GET` http://localhost:3210/i4digital/public/users/all
  
  `9.2 listar publicaciones:`  `GET` http://localhost:3210/i4digital/public/posts/all
  
  `9.3 Consultar fotos de un usuario:`  `GET` http://localhost:3210/i4digital/public/photos/user/1
  
  `9.4 Listar registros de peticiones realizadas:`  `GET` http://localhost:3210/i4digital/public/request/all
  
  `9.5 Editar registro de una petición:`  `PUT` http://localhost:3210/i4digital/public/request/update/1
  ```
  {
   "fechaPeticion": "2020-01-24",	
   "metodoPeticion": "POST",
   "datosPeticion": "Datos-informacion"
   }
   
   ```
	  
  `9.6 Eliminar registro de una petición:`  `DELETE` http://localhost:3210/i4digital/public/request/delete/1
  
  `9.7 Exportar registros en base64:`  `GET` http://localhost:3210/i4digital/public/request/report

#### 10. Decoder base64
https://base64.guru/converter/encode

Para lograr el último servicio se debe poder tener una funcionalidad que exponga un servicio el cual se usará para exportar los datos de registros, la idea es que se pueda construir un documento (excel) con los datos y enviarlos para que el frontend los pueda interpretar.

#### 11. Pruebas unitarias
 
 Las pruebas unitarias se encuentran en la carpeta test, para ejecutar las pruebas debe abrir una tercera terminal y ejecutar el siguiente comando:
   
   `$ npm run test `
   
#### 12. API desplegada en servidor externo

A continuación se encuentran los servicios de la API despleados en servidor externo, los cuales pueden ser accedidos desde un frontend o desde postman 

`12.1 listar usuarios:`  `GET` http://i4digital.cloudatadev.com/i4digital/public/users/all
  
  `12.2 listar publicaciones:`  `GET` http://i4digital.cloudatadev.com/i4digital/public/posts/all
  
  `12.3 Consultar fotos de un usuario:`  `GET` http://i4digital.cloudatadev.com/i4digital/public/photos/user/1
  
  `12.4 Listar Registros de peticiones realizadas:`  `GET` http://i4digital.cloudatadev.com/i4digital/public/request/all
  
  `12.5 Editar registro de una petición:`  `PUT` http://i4digital.cloudatadev.com/i4digital/public/request/update/1
  ```
  {
   "fechaPeticion": "2020-01-24",	
   "metodoPeticion": "POST",
   "datosPeticion": "Datos-informacion"
   }
   
   ```
	  
  `12.6 Eliminar registro de una petición:`  `DELETE` http://i4digital.cloudatadev.com/i4digital/public/request/delete/1
  
  `12.7 Exportar registros en base64:`  `GET` http://i4digital.cloudatadev.com/i4digital/public/request/report

