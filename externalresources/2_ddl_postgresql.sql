/*==============================================================*/
/* Table: peticiones                                            */
/*==============================================================*/
create table peticiones (
   cod_peticion         serial               not null,
   fecha_peticion       date                 not null,
   metodo_peticion      varchar(200)         not null,
   datos_peticion       text                 not null,
   constraint pk_peticiones primary key (cod_peticion)
);

alter table peticiones owner to user_i4digital;

