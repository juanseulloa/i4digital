import fs from 'fs';
import exceljs from 'exceljs';
import { nanoid } from 'nanoid';
import { Response } from 'express';
import pool from './../../configuracion/conexion/conexionBD';
import doc from '../../configuracion/dominios/var_documentos';
import columnas from '../../configuracion/dominios/var_informe_columnas';

class UsuarioDAO {

    protected static async peticiones(sqlConsulta: string, parametros: any, res: Response): Promise<any> {
        await pool.result(sqlConsulta, parametros)
            .then((resultado: any) => {
                res.status(200).json(resultado.rows);
            }).catch((err: any) => {
                console.log(err);
                res.status(400).json({ 'respuesta': 'Fallo en la consulta de las peticiones' });
            });
    }


    protected static async editarPeticion(sqlEditar: string, parametros: any, res: Response): Promise<any> {
        await pool.result(sqlEditar, parametros)
            .then(resultado => {
                res.status(200).json({ 'mensaje': 'Petición modificada con éxito', 'respuesta': resultado.rowCount });
            })
            .catch((err) => {
                console.log(err);
                res.status(400).json({ 'respuesta': 'Fallo en el consumo del servicio' });
            });
    }

    protected static async eliminarPeticion(sqlEliminar: string, parametros: any, res: Response): Promise<any> {
        await pool.result(sqlEliminar, parametros)
            .then(resultado => {
                res.status(200).json({ 'mensaje': 'Petición eliminada', 'respuesta': resultado.rowCount });
            })
            .catch((err) => {
                console.log(err);
                res.status(400).json({ 'respuesta': 'Fallo en el consumo del servicio' });
            });
    }

    public static async generarInformeExcel(sql: string, parametros: any, res: Response): Promise<any> {
        await pool.result(sql, parametros).then((resultado) => {
            const documento = new exceljs.Workbook();
            const hoja = documento.addWorksheet("Peticiones");
            hoja.columns = columnas.COLUNAS_PETICIONES;
            resultado.rows.map((fila) => {
                hoja.addRow(fila);
            });
            hoja.eachRow(function (filita) {
                if (filita.getCell(15).value && filita.getCell(15).value != 'Activo') {
                    filita.getCell(15).font = { color: { argb: "EC3434" } };
                }
            });
            hoja.getRow(1).eachCell((celda) => {
                celda.fill = {
                    type: 'pattern',
                    pattern: 'solid',
                    fgColor: { argb: '042940' },
                    bgColor: { argb: '042940' }
                }
                celda.font = {
                    name: 'Calibri',
                    size: 12,
                    bold: true,
                    color: { argb: 'DBF227' }
                }
            });
            this.generarArchivo(documento, res);
        }).catch((err: any) => {
            console.log(err);
            res.status(404).json({ 'Respuesta': 'Error en la consulta del informe' });
        });
    }


    public static async generarArchivo(documento: any, res: Response): Promise<any> {
        try {
            let nombrePrivado = 'InformePeticiones_' + nanoid(5) + '.xlsx';
            let base = '';
            const ruta = doc.rutaInformes + '/' + nombrePrivado;
            await documento.xlsx.writeFile(ruta);
            if (fs.existsSync(ruta)) {
                /*  res.download(ruta, function (err) {
                     if (err) { console.error(err); }
                     fs.unlink(ruta, function (error) {
                         if (error) { console.log('Error al eliminar informe excel'); }
                     });
                 }); */
                base = fs.readFileSync(ruta, 'base64');
                res.status(200).json({ respuesta: 'archivo excel en base64', base64: base })
            } else {
                res.status(406).json({ 'Respuesta': 'El archivo no existe' });
            }
        } catch (err) {
            console.log(err);
            res.status(400).json({ 'Respuesta': 'Error en la construccion del documento' });
        }
    }
}





export default UsuarioDAO;
