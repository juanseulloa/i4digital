import axios from 'axios';
import { Response } from 'express';
import pool from '../../configuracion/conexion/conexionBD';
import { api } from './../../configuracion/dominios/api_consumo';
import { SQL_PETICION } from './../../repositorios/peticiones_sql';

class UsuarioDAO {

    protected static async obtenerUsuarios(parametros: any, res: Response): Promise<any> {
        let url = api + 'users';
        await axios.get(url).then((usuarios) => {
            parametros.push(usuarios.data);
        }).then(() => {
            pool.result(SQL_PETICION.CREAR, parametros)
                .then(() => {
                    res.status(200).json(parametros[1]);
                }).catch((err) => {
                    console.log(err);
                    res.status(400).json({ respuesta: 'Fallo al registrar la petición' });
                });
        }).catch((err) => {
            console.log(err);
            res.status(400).json({ respuesta: 'Fallo al consumir la api' });
        });

    }
}





export default UsuarioDAO;
