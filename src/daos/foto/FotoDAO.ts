import axios from 'axios';
import { Response } from 'express';
import pool from '../../configuracion/conexion/conexionBD';
import { api } from '../../configuracion/dominios/api_consumo';
import { SQL_PETICION } from './../../repositorios/peticiones_sql';

class FotoDAO {

    protected static async fotosUsuario(parametros: any, res: Response): Promise<any> {
        let urlAlbum = api + 'users/' + parametros[1] + '/albums';
        let urlFotos = '';
        let arregloFotos: any[];
        arregloFotos = [];
        const albums = await axios.get(urlAlbum);
        await Promise.all(
            albums.data.map(async (album: any) => {
                urlFotos = api + 'albums/' + album.id + '/photos';
                const fotos = await axios.get(urlFotos);
                arregloFotos.push(fotos.data);
            }));
        parametros.splice(1, 1);
        parametros.push(arregloFotos);
        await pool.result(SQL_PETICION.CREAR, (parametros))
            .then(() => {
                res.status(200).json(parametros[1]);
            }).catch((err) => {
                console.log(err);
                res.status(400).json({ respuesta: 'Fallo al registrar la petición' });
            });
    }


}// end class




export default FotoDAO;
