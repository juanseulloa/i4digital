import pgPromise from 'pg-promise';
import {opcionesPG} from './opcionesConexion';
import variablesConexion from '../dominios/var_basedatos';

const pgp = pgPromise(opcionesPG);
const pool = pgp(variablesConexion);

pool.connect().then(con => {
  console.log('conexion establecida con la base: ', variablesConexion.database);
  con.done();
}).catch(error => {
  if (error.code == '3D000') {
    console.log('No existe la base de datos  ', variablesConexion.database);
  }
  if (error.code == '28P01') {
    console.log('usuario no válido ', variablesConexion.user);
  }
  if (error.code == 'ENOFOUND') {
    console.log('error servidor ');
  }

  console.log('codigo error: ', error.code);
})
export default pool;
