import cors from 'cors';
import morgan from 'morgan';
import express from 'express';

import rutasFotoAPI from '../../rutas/FotoRutas';
import rutasUsuarioAPI from '../../rutas/UsuarioRutas';
import rutasPeticionAPI from '../../rutas/PeticionRutas';
import rutasPublicacionAPI from '../../rutas/PublicacionRutas';
import rutasDocumentacion from '../../rutas/DocumentacionRutas';
import rutasInicio from '../../rutas/InicioRutas';
import InicioRutas from '../../rutas/InicioRutas';


class Servidor {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.iniciarConfiguracion();
        this.activarRutas();
    }

    public iniciarConfiguracion(): void {
        this.app.set('PORT', 3210);
        this.app.use(cors());
        this.app.use(morgan('dev'));
        this.app.use(express.json({ limit: '100mb' }));
        this.app.use(express.urlencoded({
            extended: true,
        }));
    }

    public activarRutas(): void {
        this.app.use('', InicioRutas);
        this.app.use('/i4digital/documentation', rutasDocumentacion);

        this.app.use('/i4digital/public/photos', rutasFotoAPI);
        this.app.use('/i4digital/public/users', rutasUsuarioAPI);
        this.app.use('/i4digital/public/posts', rutasPublicacionAPI);

        this.app.use('/i4digital/public/request', rutasPeticionAPI);
    }

    public iniciarServidor(): void {
        this.app.listen(this.app.get('PORT'), () => {
            console.log('servidor funcionando en el puerto: ', this.app.get('PORT'));
        });
    }

}

export default Servidor;
