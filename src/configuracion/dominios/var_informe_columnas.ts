export default {
    COLUNAS_PETICIONES: [
        { header: "Codigo", key: "codPeticion", width: 10 },
        { header: "Fecha", key: "fechaPeticion", width: 25 },
        { header: "Datos", key: "datosPeticion", width: 175 },
    ]
}
