export const SQL_PETICION = {
    OBTENER: 'SELECT cod_peticion, fecha_peticion, metodo_peticion, datos_peticion \
    FROM peticiones  \
    ORDER BY cod_peticion DESC',

    CREAR: 'INSERT INTO peticiones (metodo_peticion, fecha_peticion, datos_peticion) \
    VALUES ($1,CURRENT_DATE, $2)',

    MODIFICAR: 'UPDATE peticiones SET fecha_peticion = $1, metodo_peticion=$2, datos_peticion=$3 \
    WHERE cod_peticion = $4',

    ELIMINAR: 'DELETE FROM peticiones \
    WHERE cod_peticion = $1',

}
