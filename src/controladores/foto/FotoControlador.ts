import { Request, Response } from 'express';
import FotoDAO from '../../daos/foto/FotoDAO';

class FotoControlador extends FotoDAO {

    public obtenerFotosUsuario(req: Request, res: Response): void {
        if (!isNaN(Number(req.params.codigoUsuario))) {
            const codigoUsuario = Number(req.params.codigoUsuario);
            const metodoPeticion = req.method;
            FotoControlador.fotosUsuario([metodoPeticion, codigoUsuario], res);
        } else {
            res.status(400).json({ respuesta: 'Codigo de usuario no valido' })
        }
    }

}

const fotoControlador = new FotoControlador();
export default fotoControlador;
