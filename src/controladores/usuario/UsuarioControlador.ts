import { Request, Response } from 'express';
import UsuarioDAO from '../../daos/usuario/UsuarioDAO';
import { SQL_PETICION } from '../../repositorios/peticiones_sql';
class UsuarioControlador extends UsuarioDAO {

  // Verificación permiso
  public obtenerUsuarios(req: Request, res: Response): void {
    const metodoPeticion = req.method;
    UsuarioControlador.obtenerUsuarios([metodoPeticion], res);
  }

}

const usuarioControlador = new UsuarioControlador();
export default usuarioControlador;
