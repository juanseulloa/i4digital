import { Request, Response } from 'express';
import PeticionDAO from '../../daos/peticion/PeticionDAO';
import { SQL_PETICION } from '../../repositorios/peticiones_sql';
class PeticionControlador extends PeticionDAO {

    public obtenerPeticion(req: Request, res: Response): void {
        PeticionControlador.peticiones(SQL_PETICION.OBTENER, [], res);
    }


    public editarPeticion(req: Request, res: Response): void {
        if (!isNaN(Number(req.params.codPeticion))) {
            const codigoPeticion = Number(req.params.codPeticion);
            const fecha = req.body.fechaPeticion;
            const metodo = req.body.metodoPeticion;
            const datos = req.body.datosPeticion;
            const parametros = [fecha, metodo, datos, codigoPeticion];
            PeticionControlador.editarPeticion(SQL_PETICION.MODIFICAR, parametros, res);
        } else {
            res.status(400).json({ respuesta: 'Codigo de petición no valido' })
        }
    }

    public eliminarPeticion(req: Request, res: Response): void {
        if (!isNaN(Number(req.params.codPeticion))) {
            const codigoPeticion = Number(req.params.codPeticion);
            PeticionControlador.eliminarPeticion(SQL_PETICION.ELIMINAR, [codigoPeticion], res);
        } else {
            res.status(400).json({ respuesta: 'Codigo de petición no valido' })
        }
    }

    public generarDocumentoExcel(req: Request, res: Response): void {
        PeticionControlador.generarInformeExcel(SQL_PETICION.OBTENER, [], res);
    }

}

const peticionControlador = new PeticionControlador();
export default peticionControlador;
