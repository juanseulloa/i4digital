import { Request, Response } from 'express';
import PublicacionDAO from '../../daos/publicacion/PublicacionDAO';

class PublicacionControlador extends PublicacionDAO {

    public obtenerPublicaciones(req: Request, res: Response): void {
        const metodoPeticion = req.method;
        PublicacionControlador.publicaciones([metodoPeticion], res);
    }

}

const publicacionControlador = new PublicacionControlador();
export default publicacionControlador;
