import { Router } from 'express';
import fotoControlador from '../controladores/foto/FotoControlador';


class FotoRutas {
    public rutasFotoAPI: Router;
    constructor() {
        this.rutasFotoAPI = Router();
        this.config();
    }

    public config(): void {
        this.rutasFotoAPI.get('/user/:codigoUsuario', fotoControlador.obtenerFotosUsuario);
    }
}
const fotoRutas = new FotoRutas();
export default fotoRutas.rutasFotoAPI;
