import { Router } from 'express';

class DocumentacionRutas {
    public rutasDocumentacion: Router;
    constructor() {
        this.rutasDocumentacion = Router();
        this.config();
    }
    
    public config(): void {
        let rutaIndex = __dirname.split('build')[0] + 'documentation\\index.html';
        this.rutasDocumentacion.get('', (req, res) => {
            res.status(200).sendFile(rutaIndex)
        });
    }
}
const documentacionRutas = new DocumentacionRutas();
export default documentacionRutas.rutasDocumentacion;
