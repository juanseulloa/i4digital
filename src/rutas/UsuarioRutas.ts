import { Router } from 'express';
import usuarioControlador from '../controladores/usuario/UsuarioControlador';


class UsuariosRutas {
    public rutasUsuarioAPI: Router;
    constructor() {
        this.rutasUsuarioAPI = Router();
        this.config();
    }

    public config(): void {
        this.rutasUsuarioAPI.get('/all', usuarioControlador.obtenerUsuarios);
    }
}
const usuariosRutas = new UsuariosRutas();
export default usuariosRutas.rutasUsuarioAPI;
