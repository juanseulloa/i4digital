import { Router } from 'express';

class InicioRutas {
    public rutasInicio: Router;
    constructor() {
        this.rutasInicio = Router();
        this.config();
    }

    public config(): void {
        let rutaIndex = __dirname.split('build')[0] + 'externalresources\\4_respuestainicio.json';
        this.rutasInicio.get('', (req, res) => {
            res.status(200).sendFile(rutaIndex)
        });
        this.rutasInicio.get('/i4digital', (req, res) => {
            res.status(200).sendFile(rutaIndex)
        });
        this.rutasInicio.get('/i4digital/public', (req, res) => {
            res.status(200).sendFile(rutaIndex)
        });
    }
}
const inicioRutas = new InicioRutas();
export default inicioRutas.rutasInicio;
