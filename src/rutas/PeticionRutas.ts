import { Router } from 'express';
import peticionControlador from '../controladores/peticion/PeticionControlador';

class PeticionRutas {
    public rutasPeticionAPI: Router;
    constructor() {
        this.rutasPeticionAPI = Router();
        this.config();
    }

    public config(): void {
        this.rutasPeticionAPI.get('/all', peticionControlador.obtenerPeticion);
        this.rutasPeticionAPI.put('/update/:codPeticion', peticionControlador.editarPeticion);
        this.rutasPeticionAPI.delete('/delete/:codPeticion', peticionControlador.eliminarPeticion);
        this.rutasPeticionAPI.get('/report', peticionControlador.generarDocumentoExcel);
    }
}
const peticionRutas = new PeticionRutas();
export default peticionRutas.rutasPeticionAPI;
