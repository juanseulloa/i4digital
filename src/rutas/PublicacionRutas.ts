import { Router } from 'express';
import publicacionControlador from '../controladores/publicacion/PublicacionControlador';


class PublicacionRutas {
    public rutasPublicacionAPI: Router;
    constructor() {
        this.rutasPublicacionAPI = Router();
        this.config();
    }

    public config(): void {
        this.rutasPublicacionAPI.get('/all', publicacionControlador.obtenerPublicaciones);
    }
}
const publicacionRutas = new PublicacionRutas();
export default publicacionRutas.rutasPublicacionAPI;
