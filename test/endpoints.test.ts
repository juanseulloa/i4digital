import request from "supertest";
var apiUsers = request("http://localhost:3210/i4digital/public/users");
var apiPosts = request("http://localhost:3210/i4digital/public/posts");
var apiPhotos = request("http://localhost:3210/i4digital/public/photos");

describe('usuarios', () => {
    describe('get', () => {
        it('retorna un arreglo con objetos user en formato JSON', (done) => {
            apiUsers.get('/all')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

    })
});

describe('publicaciones', () => {
    describe('get', () => {
        it('retorna un arreglo con objetos post en formato JSON', (done) => {
            apiPosts.get('/all')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

    })
});

describe('Fotos', () => {
    describe('get', () => {
        it('retorna un arreglo con objetos photo en formato JSON', (done) => {
            let codigoUsuario = 1;
            apiPhotos.get('/user/' + codigoUsuario)
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

    })
});